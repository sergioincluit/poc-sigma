"""
Precondiciones que pueden ser utilizadas en todos los casos de prueba
"""
import pytest

from pages.login_page import LoginPageSIGMA


@pytest.fixture()
def open_login_page_SIGMA(driver):
    login_page = LoginPageSIGMA()
    login_page.go()
    assert login_page.is_displayed()
    return login_page
