import time

import allure
import pytest

from pages.abrir_cotizador_page import AbrirCotizadorSigma
from pages.costo_del_seguro_page import CostoDelSeguroPage
from pages.cotizar_seguro_automotor_page import CotizarSeguroAutomotorPage
from pages.enviar_cotizacion_como_solicitud_page import EnviarCotizacionComoSolicitudPage
from pages.identificacion_cliente_page import IdentificacionClientePage
from pages.informacion_del_vehiculo_page import InformacionDelVehiculoPage
from pages.seleccionar_marca_modelo_cotizador_page import MarcaModeloCotizadorPage
from pages.seleccionar_una_opcion_de_envio_page import SeleccionarUnaOpcionDeEnvioPage
from pages.solicitud_de_descuento_page import SolicitudDeDescuentoExtraordinarioPage
from utils.data_loader import DataLoader

pytestmark = [
    allure.parent_suite('POCSIGMA'),
    allure.suite('Secciones comunes'),
    allure.sub_suite('caso-base')
]


@pytest.mark.POCSIGMA
@pytest.mark.regression
class TestCotizarAutoCasoBase:
    """
    En esta clase se encuentran los Casos de prueba que verifican el login
    """

    def setup_class(self):
        self._data = DataLoader.get_data_from_ini_file("user_data.ini")

    def test_ejecutar_caso_base(self, open_login_page_SIGMA):
        """
        Test: Verificar elementos en la pagina de login
            El objetivo del test es verificar que se muestren todos los elementos requeridos en la pagina de login y que tengan los textos esperados

        Precondiciones:
            - El usuario tiene que estar en la pagina de login

        Steps:
            - Visualizar la pantalla de login

        Resultado esperado:
            - Se visualizan en la pagina los siguientes elementos:
                - Campo para ingresar el usuario con el placeholder "Nombre de usuario, email o DNI"
                - Campo para ingresar contraseña con el placeholder "Contraseña"
                - Boton para hacer visible la contraseña
                - Boton para iniciar sesión con el texto "Iniciar sesión"
        """
        login_page = open_login_page_SIGMA
        assert login_page.is_displayed()
        home_page = login_page.login(self._data.usuario, self._data.contrasena)
        assert home_page.is_displayed()
        #home_page.alerta_validacion()
        cotizar = AbrirCotizadorSigma()
        assert cotizar.is_displayed()
        cotizar.comenzar_cotizacion_auto()
        modelo_año_auto = MarcaModeloCotizadorPage()
        assert modelo_año_auto.is_displayed()
        modelo_año_auto.ingresa_modelo("GOL")
        modelo_año_auto.ingresa_año(2016)
        modelo_año_auto.buscar_auto()
        modelo_año_auto.seleccionar_auto()
        cotizar_seguro = CotizarSeguroAutomotorPage()
        assert cotizar_seguro.is_displayed()
        cotizar_seguro.buscar_cliente()
        cargar_cliente = IdentificacionClientePage()
        assert cargar_cliente.is_displayed()
        cargar_cliente.seleccionar_cliente("35414241")
        cargar_cliente.seleccionar_aceptar()
        cotizar_seguro.deseleccionar_coberturas_no_necesarias()
        cotizar_seguro.seleccionar_vigencia()
        cotizar_seguro.seleccionar_plan_de_pago()
        cotizar_seguro.seleccionar_cotizar()
        descuento = SolicitudDeDescuentoExtraordinarioPage()
        assert descuento.is_displayed()
        descuento.aplicar()
        costo_seguro = CostoDelSeguroPage()
        assert costo_seguro.is_displayed()
        costo_seguro.enviar_solicitud()
        enviar_cotizacion = EnviarCotizacionComoSolicitudPage()
        assert enviar_cotizacion.is_displayed()
        enviar_cotizacion.editar_vehiculo()
        guarda_datos_vehiculo = InformacionDelVehiculoPage()
        assert guarda_datos_vehiculo.is_displayed()
        guarda_datos_vehiculo.carga_guarda_datos_vehiculo("asdasdasdasdasd","qweqweqweqweqwe")
        enviar_cotizacion.seleccionar_boton_enviar()
        emitir_poliza = SeleccionarUnaOpcionDeEnvioPage()
        assert emitir_poliza.is_displayed()
        time.sleep(8)