from flask import Flask
import subprocess

app = Flask(__name__)

@app.route('/')
def holamundo():
    subprocess.run(["pytest", "-v", "-m", "POCSIGMA"])
    return 'Hola Mundo!'

if __name__ == '__main__':
    app.run()
