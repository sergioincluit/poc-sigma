import string
import random

class RandomPatent:

    def __init__(self):
        super().__init__()

    def cambioDePatente(self):
        letras = ''.join(random.choices(string.ascii_uppercase, k=3))
        numeros = ''.join(random.choices(string.digits, k=3))
        patente = letras + str(numeros)

        return patente

