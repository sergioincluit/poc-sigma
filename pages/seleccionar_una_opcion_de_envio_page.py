import logging

from selenium.webdriver.common.by import By

from pages.base_page import BasePage
from utils.web_driver_actions import locator_by


class SeleccionarUnaOpcionDeEnvioPage(BasePage):

    __PANTALLA_SELECCIONAR_UNA_OPCION_DE_ENVIO = locator_by({
        'BY': By.ID,
        'LOCATOR': 'idEmisionOpcion'
    })

    __BUTTON_ACEPTAR = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//button[@class='boton color-resaltado idBotonAceptar']"
    })

    def __init__(self):
        super().__init__()

    def is_displayed(self):
        try:
            self.wait_for_element(self.__PANTALLA_SELECCIONAR_UNA_OPCION_DE_ENVIO)
            return True
        except:
            logging.ERROR("No se visualiza la pantalla llamada Seleccionar una opción de envío.")
            return False
