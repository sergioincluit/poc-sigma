import logging

from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By

from pages.base_page import BasePage
from utils.web_driver_actions import locator_by


class CotizarSeguroAutomotorPage(BasePage):

    __PANTALLA_COTIZAR_SEGURO_AUTOMOTOR = locator_by({
        'BY': By.ID,
        'LOCATOR': 'cotizarSeguroAuto'
    })

    __BUSCAR_CLIENTE = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//button[contains(@class,'idBuscarCliente')]"
    })

    __BUTTON_ACEPTAR = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//button[@class='boton color-resaltado']"
    })

    __BUTTON_COTIZAR = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//button[contains(@class,'idCotizarSABtnAcep')]"
    })

    __COBERTURA_B1 = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//label[contains(@for,'HATSFormin5E_1342_4')]"
    })

    __COBERTURA_C = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//label[contains(@for,'HATSFormin5E_1216_3')]"
    })

    __COBERTURA_A = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//label[contains(@for,'HATSFormin5E_1204_3')]"
    })

    __COBERTURA_C1 = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//label[contains(@for,'HATSFormin5E_1348_4')]"
    })

    __COBERTURA_D2 = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//label[contains(@for,'HATSFormin5E_1366_32')]"
    })

    __COBERTURA_M = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//label[contains(@for,'HATSFormin5E_1405_7')]"
    })

    __PLAN_DE_PAGO = locator_by({
        'BY': By.NAME,
        'LOCATOR': 'in_1862_2'
    })

    __VIGENCIA_PAGO = locator_by({
        'BY': By.NAME,
        'LOCATOR': 'in_1598_1'
    })

    __PLAN_DE_COBERTURA = locator_by({
        'BY': By.NAME,
        'LOCATOR': 'in_2588_1'
    })

    def __init__(self):
        super().__init__()

    def is_displayed(self):
        try:
            self.wait_for_element(self.__PANTALLA_COTIZAR_SEGURO_AUTOMOTOR)
            return True
        except:
            logging.ERROR("No se visualiza la pantalla llamada Cotizar seguro automotor.")
            return False

    def buscar_cliente(self):
        self.wait_for_element(self.__BUSCAR_CLIENTE)
        self.click_element(self.__BUSCAR_CLIENTE)

    def seleccionar_aceptar(self):
        self.wait_for_element(self.__BUTTON_ACEPTAR)
        self.click_element(self.__BUTTON_ACEPTAR)

    def _destildar_cobertura_a(self):
        self.wait_for_element(self.__COBERTURA_A)
        self.click_element(self.__COBERTURA_A)

    def _destildar_cobertura_b1(self):
        self.wait_for_element(self.__COBERTURA_B1)
        self.click_element(self.__COBERTURA_B1)

    def seleccionar_cobertura_b1(self):
        self._destildar_cobertura_b1()

    def _destildar_cobertura_c(self):
        self.wait_for_element(self.__COBERTURA_C)
        self.click_element(self.__COBERTURA_C)

    def _destildar_cobertura_c1(self):
        self.wait_for_element(self.__COBERTURA_C1)
        self.click_element(self.__COBERTURA_C1)

    def tildar_cobertura_d2(self):
        self.wait_for_element(self.__COBERTURA_D2)
        self.click_element(self.__COBERTURA_D2)

    def tildar_cobertura_M(self):
        self.wait_for_element(self.__COBERTURA_M)
        self.click_element(self.__COBERTURA_M)

    def deseleccionar_coberturas_no_necesarias(self, iscasobase2=False):
        if not iscasobase2:
            self._destildar_cobertura_b1()
            self._destildar_cobertura_c()
            self._destildar_cobertura_c1()
        else:
            self._destildar_cobertura_a()
            self._destildar_cobertura_b1()
            self._destildar_cobertura_c()
            self._destildar_cobertura_c1()

    def seleccionar_cotizar(self):
        self.scroll_to_the_bottom_of_the_page()
        self.wait_for_element(self.__BUTTON_COTIZAR)
        self.click_element(self.__BUTTON_COTIZAR)

    def seleccionar_plan_de_pago(self):
        self.wait_for_element(self.__PLAN_DE_PAGO)
        self.select_option(self.__PLAN_DE_PAGO, "3 cuotas")

    def seleccionar_vigencia(self):
        self.wait_for_element(self.__VIGENCIA_PAGO)
        self.select_option(self.__VIGENCIA_PAGO, "Cuatrimestral")

    def seleccionar_plan_de_cobertura(self):
        self.wait_for_element(self.__PLAN_DE_COBERTURA)
        self.select_option(self.__PLAN_DE_COBERTURA, "Elegir")
