import logging

from selenium.webdriver.common.by import By

from pages.base_page import BasePage
from utils.config_loader import read_config_from_current_env
from utils.web_driver_actions import locator_by


class HomePageSigma(BasePage):

    __USER_LOGGED_LBL = locator_by({
        'BY': By.ID,
        'LOCATOR': 'navbarTop'
    })

    __ALERTA_VALIDACION_MAIL = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//div[@class='modal-alerta alerta-info text-left con-cerrar con-titulo con-botones']//button[@class='close']"
    })

    def __init__(self):
        super().__init__()

    def is_displayed(self):
        try:
            self.wait_for_element(self.__USER_LOGGED_LBL)
            return True
        except:
            logging.ERROR("No se visualiza la pantalla del Home Page.")
            return False

    def alerta_validacion(self):
        self.wait_for_element(self.__ALERTA_VALIDACION_MAIL)
        self.click_element(self.__ALERTA_VALIDACION_MAIL)

