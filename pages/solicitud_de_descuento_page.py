import logging

from selenium.webdriver.common.by import By

from pages.base_page import BasePage
from utils.config_loader import read_config_from_current_env
from utils.web_driver_actions import locator_by


class SolicitudDeDescuentoExtraordinarioPage(BasePage):

    __PANTALLA_SOLICITUD_DESCUENTO_EXTRAORDINARIO = locator_by({
        'BY': By.ID,
        'LOCATOR': 'solicitudDescExt'
    })

    __BUTTON_ACEPTAR = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//button[contains(@class,'idSolicitudDE')]"
    })

    def __init__(self):
        super().__init__()

    def is_displayed(self):
        try:
            self.wait_for_element(self.__PANTALLA_SOLICITUD_DESCUENTO_EXTRAORDINARIO)
            return True
        except:
            logging.ERROR("No se visualiza la pantalla llamada Solicitud de descuento extraordniario.")
            return False

    def aplicar(self):
        self.scroll_to_the_bottom_of_the_page()
        self.wait_for_element(self.__BUTTON_ACEPTAR)
        self.click_element(self.__BUTTON_ACEPTAR)
