import logging

from selenium.webdriver.common.by import By

from pages.base_page import BasePage
from utils.web_driver_actions import locator_by


class FotosPage(BasePage):

    __FOTOS = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//button[@class='tabbar__button tabbar--material__button tabbar--top__button']//div[contains(text(),'Fotos')]"
    })

    __FOTO_LATERAL = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//div[@data-id='Lateral Conductor']//input[@type='file']"
    })

    __FOTO_LATERAL_ACOMPANANTE = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//div[@data-id='Lateral Acompañante']//input[@type='file']"
    })

    __FOTO_FRENTE = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//div[@data-id='Frente']//input[@type='file']"
    })

    __FOTO_TRASERA = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//div[@data-id='Trasera']//input[@type='file']"
    })

    __ENVIAR = locator_by({
        'BY': By.ID,
        'LOCATOR': "enviar"
    })

    def __init__(self):
        super().__init__()

    def is_displayed(self):
        try:
            self.wait_for_element(self.__FOTOS)
            self.wait_for_element(self.__FOTO_LATERAL)
            return True
        except:
            logging.ERROR("No se visualiza la pantalla llamada Seleccionar una opción de envío.")
            return False


    def button_fotos(self):
        self.wait_for_element(self.__FOTOS)
        self.click_element(self.__FOTOS)

    def foto_lateral(self):
        self.wait_for_element(self.__FOTO_LATERAL)
        self.upload_photo(self.__FOTO_LATERAL, "fotos_test/foto_para_segundo_caso_base.jpg")

    def foto_lateral_acompanante(self):
        self.wait_for_element(self.__FOTO_LATERAL_ACOMPANANTE)
        self.upload_photo(self.__FOTO_LATERAL_ACOMPANANTE, "fotos_test/foto_para_segundo_caso_base.jpg")

    def foto_frente(self):
        self.wait_for_element(self.__FOTO_FRENTE)
        self.upload_photo(self.__FOTO_FRENTE, "fotos_test/foto_para_segundo_caso_base.jpg")

    def foto_trasera(self):
        self.wait_for_element(self.__FOTO_TRASERA)
        self.upload_photo(self.__FOTO_TRASERA, "fotos_test/foto_para_segundo_caso_base.jpg")

    def enviar(self):
        self.wait_for_element(self.__ENVIAR)
        self.click_element(self.__ENVIAR)

