import logging

from selenium.webdriver.common.by import By

from pages.base_page import BasePage
from utils.config_loader import read_config_from_current_env
from utils.web_driver_actions import locator_by


class MarcaModeloCotizadorPage(BasePage):

    __PANTALLA_SELECCIONAR_MARCA_MODELO = locator_by({
        'BY': By.ID,
        'LOCATOR': 'cotizarMM'
    })

    __INPUT_MODELO_AUTO = locator_by({
        'BY': By.ID,
        'LOCATOR': 'in_89_30'
    })

    __INPUT_AÑO_AUTO = locator_by({
        'BY': By.ID,
        'LOCATOR': 'in_125_4'
    })

    __BUSCAR_AUTO = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//button[contains(@class,'idBuscarAuto')]"
    })

    __SELECT_AUTO = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//div[contains(@id,'idSelectAuto')]//select[@name='in_322_1']//option[@value='1']"
    })
    def __init__(self):
        super().__init__()

    def is_displayed(self):
        try:
            self.wait_for_element(self.__PANTALLA_SELECCIONAR_MARCA_MODELO)
            return True
        except:
            logging.ERROR("No se visualiza la pantalla llamada Seleccioná marca y modelo del vehículo a cotizar")
            return False

    def ingresa_modelo(self, modelo):
        self.wait_for_element(self.__INPUT_MODELO_AUTO)
        self.send_keys_to_element(self.__INPUT_MODELO_AUTO, modelo)

    def ingresa_año(self, año):
        self.wait_for_element(self.__INPUT_AÑO_AUTO)
        self.send_keys_to_element(self.__INPUT_AÑO_AUTO, año)

    def buscar_auto(self):
        self.wait_for_element(self.__BUSCAR_AUTO)
        self.click_element(self.__BUSCAR_AUTO)

    def seleccionar_auto(self):
        self.wait_for_element(self.__SELECT_AUTO)
        self.click_element(self.__SELECT_AUTO)
