import logging

from selenium.webdriver.common.by import By

from pages.base_page import BasePage
from utils.web_driver_actions import locator_by


class IdentificacionClientePage(BasePage):

    __PANTALLA_IDENTIFICACION_CLIENTE = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//span[contains(@class,'idIdentCliente')]"
    })

    __INPUT_DNI = locator_by({
        'BY': By.ID,
        'LOCATOR': 'in_542_10'
    })

    __SELECT_SEXO = locator_by({
        'BY': By.NAME,
        'LOCATOR': 'in_806_1'
    })

    __BUTTON_ACEPTAR = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//button[contains(@class,'idIdentClienteBtnAcep')]"
    })

    __BUTTON_ACEPTAR_CON_DATOS_CARGADOS = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//button[contains(@class,'idClienteDatosDni')]"
    })

    def __init__(self):
        super().__init__()

    def is_displayed(self):
        try:
            self.wait_for_element(self.__PANTALLA_IDENTIFICACION_CLIENTE)
            return True
        except:
            logging.ERROR("No se visualiza la pantalla llamada Identificación cliente.")
            return False

    def _cargar_dni(self, dni):
        self.wait_for_element(self.__INPUT_DNI)
        self.send_keys_to_element(self.__INPUT_DNI, dni)

    def _cargar_sexo(self):
        self.wait_for_element(self.__SELECT_SEXO)
        self.select_option(self.__SELECT_SEXO, "Masculino")

    def seleccionar_aceptar(self):
        self.wait_for_element(self.__BUTTON_ACEPTAR_CON_DATOS_CARGADOS)
        self.click_element(self.__BUTTON_ACEPTAR_CON_DATOS_CARGADOS)

    def seleccionar_cliente(self, dni):
        self._cargar_dni(dni)
        self._cargar_sexo()
        self.wait_for_element(self.__BUTTON_ACEPTAR)
        self.click_element(self.__BUTTON_ACEPTAR)
