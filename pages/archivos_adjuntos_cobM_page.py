import logging

from selenium.webdriver.common.by import By

from pages.base_page import BasePage
from utils.web_driver_actions import locator_by


class ArchivosAdjuntosCobM(BasePage):

    __PANTALLA_ARCHIVOS_ADJUNTOS_4 = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//div[@class='pan-encabezado']"
    })

    __TIPO_DE_ADJUNTO = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//select[contains(@id,'notas1')]//option[@value='Póliza']"
    })

    __ADJUNTO = locator_by({
        'BY': By.ID,
        'LOCATOR': 'datos1'
    })

    __ACEPTAR_4 = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//input[@class='boton color-resaltado']"
    })

    def __init__(self):
        super().__init__()

    def is_displayed(self):
        try:
            self.wait_for_element(self.__PANTALLA_ARCHIVOS_ADJUNTOS_4)
            return True
        except:
            logging.ERROR("No se visualiza la pantalla llamada Archivos Adjuntos.")
            return False

    def tipo_de_adjunto(self):
        self.wait_for_element(self.__TIPO_DE_ADJUNTO)
        self.click_element(self.__TIPO_DE_ADJUNTO)

    def adjunto(self):
        self.wait_for_element(self.__ADJUNTO)
        self.upload_photo(self.__ADJUNTO, "polizas_pdf_test/PolizasVigentes-91950.pdf")

    def aceptar_4(self):
        self.wait_for_element(self.__ACEPTAR_4)
        self.click_element(self.__ACEPTAR_4)
