import logging

from selenium.webdriver.common.by import By

from pages.base_page import BasePage
from utils.web_driver_actions import locator_by


class AbrirCotizadorSigma(BasePage):

    __MENU_COTIZAR = locator_by({
        'BY': By.ID,
        'LOCATOR': 'navCotizadores'
    })

    __COTIZAR_AUTO = locator_by({
        'BY': By.ID,
        'LOCATOR': 'cotizarAuto'
    })

    def __init__(self):
        super().__init__()

    def is_displayed(self):
        try:
            self.wait_for_element(self.__MENU_COTIZAR)
            return True
        except:
            logging.ERROR("No se visualiza la pantalla del Cotizador.")
            return False

    def seleccionar_menu_cotizar(self):
        self.click_element(self.__MENU_COTIZAR)

    def seleccionar_cotizar_auto(self):
        self.wait_for_element(self.__COTIZAR_AUTO)
        self.click_element(self.__COTIZAR_AUTO)

    def comenzar_cotizacion_auto(self):
        self.seleccionar_menu_cotizar()
        self.seleccionar_cotizar_auto()
