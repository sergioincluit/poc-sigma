import logging

from selenium.webdriver.common.by import By

from pages.base_page import BasePage
from utils.web_driver_actions import locator_by


class EnviarCotizacionComoSolicitudPage(BasePage):

    __PANTALLA_ENVIAR_COTIZACION_COMO_SOLICITUD = locator_by({
        'BY': By.ID,
        'LOCATOR': 'cotizacionSolicitud'
    })

    __BUTTON_VEHICULO = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//button[contains(@class,'idBtnSolicitud14')]"
    })

    __BUTTON_INSPECCION = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//button[contains(@class,'idBtnSolicitud16')]"
    })

    __BUTTON_ACEPTAR = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//button[contains(@class,'idBtnSolicitud5')]"
    })

    def __init__(self):
        super().__init__()

    def is_displayed(self):
        try:
            self.wait_for_element(self.__PANTALLA_ENVIAR_COTIZACION_COMO_SOLICITUD)
            return True
        except:
            logging.ERROR("No se visualiza la pantalla llamada Enviar cotizacion como solicitud.")
            return False

    def editar_vehiculo(self):
        self.wait_for_element(self.__BUTTON_VEHICULO)
        self.click_element(self.__BUTTON_VEHICULO)

    def editar_inspeccion(self):
        self.wait_for_element(self.__BUTTON_INSPECCION)
        self.scroll_to_the_bottom_of_the_page()
        self.click_element(self.__BUTTON_INSPECCION)

    def seleccionar_boton_enviar(self):
        self.scroll_to_the_bottom_of_the_page()
        self.wait_for_element(self.__BUTTON_ACEPTAR)
        self.click_element(self.__BUTTON_ACEPTAR)
