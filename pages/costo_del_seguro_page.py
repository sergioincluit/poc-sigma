import logging

from selenium.webdriver.common.by import By

from pages.base_page import BasePage
from utils.web_driver_actions import locator_by


class CostoDelSeguroPage(BasePage):

    __PANTALLA_COSTO_DEL_SEGURO = locator_by({
        'BY': By.ID,
        'LOCATOR': 'idCostoSeguro'

    })

    __BUTTON_ACEPTAR = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//button[contains(@class,'idBtnCostoSeguro')]"
    })

    def __init__(self):
        super().__init__()

    def is_displayed(self):
        try:
            self.wait_for_element(self.__PANTALLA_COSTO_DEL_SEGURO)
            return True
        except:
            logging.ERROR("No se visualiza la pantalla llamada Costo del seguro.")
            return False

    def enviar_solicitud(self):
        self.scroll_to_the_bottom_of_the_page()
        self.wait_for_element(self.__BUTTON_ACEPTAR)
        self.click_element(self.__BUTTON_ACEPTAR)

