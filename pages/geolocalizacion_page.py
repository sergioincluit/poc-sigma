import logging

from selenium.webdriver.common.by import By

from pages.base_page import BasePage
from utils.web_driver_actions import locator_by


class GeolocalizacionPage(BasePage):

    __POP_UP_GEOLOCALIZACION = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//div[contains(text(),'Es necesario que enciendas y aceptes la GeoLocalización')]"
    })

    __BUTTON_OK = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//ons-alert-dialog-button[@class='alert-dialog-button--primal alert-dialog-button--rowfooter alert-dialog-button alert-dialog-button--material alert-dialog-button--rowfooter--material alert-dialog-button--primal--material']"
    })

    __IFRAME = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//iframe[@frameborder='0']"
    })

    def __init__(self):
        super().__init__()

    def is_displayed(self):
        try:
            self.wait_for_element(self.__IFRAME)
            self.switch_to_iframe(self.__IFRAME)
            self.wait_for_element(self.__POP_UP_GEOLOCALIZACION)
            return True
        except:
            logging.ERROR("No se visualiza la pantalla llamada Seleccionar una opción de envío.")
            return False

    def seleccionar_ok(self):
        self.wait_for_element(self.__BUTTON_OK)
        self.click_element(self.__BUTTON_OK)