import logging

from selenium.webdriver.common.by import By

from pages.base_page import BasePage
from utils.web_driver_actions import locator_by


class ArchivosAdjuntos(BasePage):

    __PANTALLA_ARCHIVOS_ADJUNTOS = locator_by({
        'BY': By.ID,
        'LOCATOR': 'idPantalla4Fotos'
    })

    __IMAGEN_FRONTAL = locator_by({
        'BY': By.ID,
        'LOCATOR': 'formFile'
    })
    
    __IMAGEN_POSTERIOR = locator_by({
        'BY': By.ID,
        'LOCATOR': 'formFile1'
    })

    __IMAGEN_LATERAL_IZQUIERDO = locator_by({
        'BY': By.ID,
        'LOCATOR': 'formFile2'
    })

    __IMAGEN_LATERAL_DERECHO = locator_by({
        'BY': By.ID,
        'LOCATOR': 'formFile3'
    })

    __GUARDAR = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//button[@class='p-button p-button-lg w100M ng-star-inserted']"
    })

    __ALERTA_EXITO = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//span[contains(text(),'Éxito!')]"
    })

    __OK = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//button[@class='mensaje-boton ng-star-inserted']"
    })

    __ACEPTAR = locator_by({
        'BY': By.ID,
        'LOCATOR': 'idBotonAceptar'
    })

    __PANTALLA_ARCHIVOS_ADJUNTOS_4 = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//div[@class='pan-encabezado']"
    })

    __TIPO_DE_ADJUNTO = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//select[contains(@id,'notas1')]//option[@value='Póliza']"
    })

    __ADJUNTO = locator_by({
        'BY': By.ID,
        'LOCATOR': 'datos1'
    })

    __ACEPTAR_4 = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//input[@class='boton color-resaltado']"
    })





    def __init__(self):
        super().__init__()

    def is_displayed(self):
        try:
            self.wait_for_element(self.__PANTALLA_ARCHIVOS_ADJUNTOS)
            self.wait_for_element(self.__PANTALLA_ARCHIVOS_ADJUNTOS_4)
            return True
        except:
            logging.ERROR("No se visualiza la pantalla llamada Archivos Adjuntos.")
            return False

    def imagen_frontal(self):
        self.wait_for_element(self.__IMAGEN_FRONTAL)
        self.upload_photo(self.__IMAGEN_FRONTAL, "fotos_test/foto_para_segundo_caso_base.jpg")

    def imagen_posterior(self):
        self.wait_for_element(self.__IMAGEN_POSTERIOR)
        self.upload_photo(self.__IMAGEN_POSTERIOR, "fotos_test/foto_para_segundo_caso_base.jpg")

    def imagen_lateral_izquierdo(self):
        self.wait_for_element(self.__IMAGEN_LATERAL_IZQUIERDO)
        self.upload_photo(self.__IMAGEN_LATERAL_IZQUIERDO, "fotos_test/foto_para_segundo_caso_base.jpg")

    def imagen_lateral_derecho(self):
        self.wait_for_element(self.__IMAGEN_LATERAL_DERECHO)
        self.upload_photo(self.__IMAGEN_LATERAL_DERECHO, "fotos_test/foto_para_segundo_caso_base.jpg")

    def guardar(self):
        self.wait_for_element(self.__GUARDAR)
        self.click_element(self.__GUARDAR)

    def alerta_exito(self):
        try:
            self.wait_for_element(self.__ALERTA_EXITO)
            return True
        except ValueError:
            logging.error("Algo fallo al guardar las fotos")

    def seleccionar_ok(self):
        self.wait_for_element(self.__OK)
        self.click_element(self.__OK)

    def aceptar(self):
        self.wait_for_element(self.__ACEPTAR)
        self.click_element(self.__ACEPTAR)

    def tipo_de_adjunto(self):
        self.wait_for_element(self.__TIPO_DE_ADJUNTO)
        self.click_element(self.__TIPO_DE_ADJUNTO)

    def adjunto(self):
        self.wait_for_element(self.__ADJUNTO)
        self.upload_photo(self.__ADJUNTO, "fotos_test/foto_para_segundo_caso_base.jpg")

    def aceptar_4(self):
        self.wait_for_element(self.__ACEPTAR_4)
        self.click_element(self.__ACEPTAR_4)
