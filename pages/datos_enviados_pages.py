import logging

from selenium.webdriver.common.by import By

from pages.base_page import BasePage
from utils.web_driver_actions import locator_by


class DatosEnviadosPage(BasePage):

    __LOGO_MERCANTIL = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//img[@class='logo']"
    })

    __DATOS_ENVIADOS_A_LA_COMPANIA = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//h1[contains(text(),'Solicitud realizada correctamente')]"
    })

    __BOTON_SALIR = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//input[@class='ApplicationButton']"
    })

    __IFRAME = locator_by({
        'BY': By.ID,
        'LOCATOR': 'iframePedirInspeccionTecno'
    })

    def __init__(self):
        super().__init__()

    def is_displayed(self):
        try:
            self.wait_for_element(self.__LOGO_MERCANTIL)
            return True
        except:
            logging.ERROR("No se visualiza la pantalla llamada Seleccionar una opción de envío.")
            return False

    def datos_correctos(self):
        self.wait_for_element(self.__DATOS_ENVIADOS_A_LA_COMPANIA)

    def seleccionar_salir(self):
        #self.wait_for_element(self.__IFRAME)
        #self.switch_to_iframe(self.__IFRAME)
        self.switch_to_default_content_after_iframe()
        self.scroll_to_the_bottom_of_the_page()
        self.wait_for_element(self.__BOTON_SALIR)
        self.click_element(self.__BOTON_SALIR)

