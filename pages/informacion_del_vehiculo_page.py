import logging
import time

from selenium.webdriver.common.by import By

from pages.base_page import BasePage
from utils.web_driver_actions import locator_by
from utils.random_patent import RandomPatent


class InformacionDelVehiculoPage(BasePage):

    __PANTALLA_INFROMACION_DEL_VEHICULO = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//span[contains(@class,'idInfoVehiculo')]"
    })

    __INPUT_DOMINIO = locator_by({
        'BY': By.ID,
        'LOCATOR': 'in_542_20'
    })

    __INPUT_CHASIS = locator_by({
        'BY': By.ID,
        'LOCATOR': 'in_1070_17'
    })

    __INPUT_MOTOR = locator_by({
        'BY': By.ID,
        'LOCATOR': 'in_1334_25'
    })

    __BUTTON_ACEPTAR = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//button[contains(@class,'idInfoVehiculo')]"
    })

    def __init__(self):
        super().__init__()

    def is_displayed(self):
        try:
            self.wait_for_element(self.__PANTALLA_INFROMACION_DEL_VEHICULO)
            return True
        except:
            logging.ERROR("No se visualiza la pantalla llamada Información del vehículo.")
            return False

    def _cargar_dominio(self):
        self.wait_for_element(self.__INPUT_DOMINIO)
        self.clean_input(self.__INPUT_DOMINIO)
        patente = RandomPatent()
        self.send_keys_to_element(self.__INPUT_DOMINIO, patente.cambioDePatente())


    def _cargar_chasis(self, chasis):
        self.wait_for_element(self.__INPUT_CHASIS)
        self.send_keys_to_element(self.__INPUT_CHASIS, chasis)

    def _cargar_motor(self, motor):
        self.wait_for_element(self.__INPUT_MOTOR)
        self.send_keys_to_element(self.__INPUT_MOTOR, motor)

    def carga_guarda_datos_vehiculo(self, chasis, motor):
        self._cargar_dominio()
        self._cargar_chasis(chasis)
        self._cargar_motor(motor)
        self.scroll_to_the_bottom_of_the_page()
        self.wait_for_element(self.__BUTTON_ACEPTAR)
        self.click_element(self.__BUTTON_ACEPTAR)
