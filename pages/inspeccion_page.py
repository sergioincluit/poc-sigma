import logging

from selenium.webdriver.common.by import By

from pages.base_page import BasePage
from utils.web_driver_actions import locator_by


class InspeccionPage(BasePage):

    __PANTALLA_INSPECCION = locator_by({
        'BY': By.ID,
        'LOCATOR': 'idEmisionOpcion'
    })

    __BUTTON_ACEPTAR = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//button[@class='boton color-resaltado idBotonAceptar1']"
    })

    __SELECCION_AUTOINSPECCION = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//span[@class='custom-control-label' and contains(text(),'Autoinspección')]"
    })

    __SELECCION_POLIZA_DE_OTRA_CIA = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//span[@class='custom-control-label' and contains(text(),'Adjunto copia de póliza de otra Cía.')]"
    })

    def __init__(self):
        super().__init__()

    def is_displayed(self):
        try:
            self.wait_for_element(self.__PANTALLA_INSPECCION)
            return True
        except:
            logging.ERROR("No se visualiza la pantalla llamada Seleccione una opción.")
            return False

    def seleccionar_autoinspeccion(self):
        self.wait_for_element(self.__SELECCION_AUTOINSPECCION)
        self.click_element(self.__SELECCION_AUTOINSPECCION)

    def seleccionar_adjunto_copia_de_otra_cia(self):
        self.wait_for_element(self.__SELECCION_POLIZA_DE_OTRA_CIA)
        self.click_element(self.__SELECCION_POLIZA_DE_OTRA_CIA)

    def seleccion_aceptar(self):
        self.scroll_to_the_bottom_of_the_page()
        self.wait_for_element(self.__BUTTON_ACEPTAR)
        self.click_element(self.__BUTTON_ACEPTAR)

